# -*- coding:utf-8 -*-


import json
import requests
import time
from selenium import webdriver



def getCookies():
    #"./chromedriver.exe", options=opt
    opt = webdriver.ChromeOptions()
    opt.headless = True
    driver = webdriver.Chrome("./chromedriver.exe")

    url = "https://www.tgo58.com/"

    driver.get(url)

    driver.find_element_by_xpath('//*[@id="fr_username"]').send_keys('dd6115191')
    driver.find_element_by_xpath('//*[@id="fr_password"]').send_keys('mm871211')
    driver.find_element_by_xpath('//*[@id="HeaderLogin"]').click()
    time.sleep(1)
    driver.get("https://www.tgo58.com/sport/43#")
    time.sleep(1)

    driver.get("https://www.tgo58.com/loadGame/XJ/22010")
    time.sleep(2)

    cookies = driver.get_cookies()

    ncookies = {}
    for cookie in cookies:
        ncookies[cookie["name"]] = cookie["value"]
    driver.close()

    return ncookies

def getdocs(cookies):
    session = requests.session()

    # #目标url
    postUrl = 'https://xj-sb-asia-bge.prdasbbwla2.com/zh-cn/serv/getodds'

    #使用session发起请求


    # payloadData数据
    payloadData = {
    "dc": None,
    "dv": "-1",
    "ifl": True,
    "iip": True,
    "ipf": False,
    "ot": "0",
    "pid": "0",
    "pn": "0",
    "pt": "4",
    "sid": [1],
    "tz": 480,
    "ubt": "am"}

    # 请求头设置
    payloadHeader = {
    "Accept":"application/json, text/javascript, */*; q=0.01",
    "Accept-Encoding":"gzip, deflate, br",
    "Accept-Language":"zh-CN,zh;q=0.9,zh-TW;q=0.8,en-US;q=0.7,en;q=0.6",
    "Connection":"keep-alive",
    "Content-Length":"112",
    "Content-Type":"application/json; charset=UTF-8",

    "Host":"xj-sb-asia-bge.prdasbbwla2.com",
    "Origin":"https://xj-sb-asia-bge.prdasbbwla2.com",
    "Referer":"https://xj-sb-asia-bge.prdasbbwla2.com/zh-cn/sports",
    "Sec-Fetch-Dest":"empty",
    "Sec-Fetch-Mode":"cors",
    "Sec-Fetch-Site":"same-origin",
    "User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36",
    "X-Requested-With":"XMLHttpRequest",

    }

    # 下载超时
    timeOut = 25
    # 代理
    r = session.post(postUrl, data=json.dumps(payloadData), headers=payloadHeader, cookies=cookies)
    dumpJsonData = json.dumps(payloadData)

    res = session.post(postUrl, data=dumpJsonData, headers=payloadHeader, cookies=cookies, timeout=timeOut, allow_redirects=True)
    if not res.text:
        return False

    docs = json.loads(res.text)

    flag = True
    for doc in docs["i-ot"][0]["egs"]:
        for xdoc in doc["es"]:
            ztime = xdoc["i"][5]
            team = xdoc["i"][:2]
            half = xdoc["i"][12]

            if xdoc["pci"].get("ctn") == "角球" and half == "上半场":
                if xdoc["o"].get("ou") is None:
                    continue
                abig = xdoc["o"]["ou"][5]
                asmall = xdoc["o"]["ou"][7]
                try:
                    bbig = xdoc["o"]["ou"][13]
                    bsmall = xdoc["o"]["ou"][15]
                except IndexError:
                    bbig = ""
                    bsmall = ""
                print(team, ztime, half)
                print(abig, asmall, bbig, bsmall)
                if ztime >= "45:00":
                    if str(abig) == str(asmall) == "0.91":
                        print('a符合要求', team, xdoc["i"][4:6], abig, asmall)
                        flag = False
                    if str(bbig) == str(bsmall) == "0.91":
                        print('b符合要求', team, xdoc["i"][4:6], bbig, bsmall)
                        flag = False
    if flag:
        print("当前没有符合条件的比赛！")


    return True


def run():
    cookies = getCookies()

    while True:
        rs = getdocs(cookies)

        if not rs:
            cookies = getCookies()
            rs = getdocs(cookies)

        # time.sleep()

if __name__ == "__main__":
    run()