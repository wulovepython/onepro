alist = [1, 3, 5, 2, 10, 86, 54, -1]


# def maopao(alist):
#     for i in range(len(alist)):
#         for j in range(len(alist) - 1 - i):
#             if alist[j] > alist[j + 1]:
#                 alist[j], alist[j + 1] = alist[j + 1], alist[j]
#
#     print(alist)


# maopao(alist)


# for i in range(5):
#     for j in range(6):
#         print(i, '---', j)

# def choose(alist):
#     alist1 = []
#     for i in range(len(alist)):
#         for j in range(len(alist)):
#             if alist[j] == min(alist):
#                 alist1.append(alist[j])
#                 alist.remove(alist[j])
#     print(alist1)
#
# choose(alist)

# def fn():
#     a = input('please write str:')
#
#     adict = {}
#     for i in a:
#         adict[i] = 0
#     for d in adict.keys():
#         for i in a:
#             if d == i:
#                 adict[d] += 1
#     print(adict)


# lambda 匿名函数
# b = lambda x: x ** 2
# c = map(b, [1, 2, 3, 4, 5])
# for i in c:
#     print(i)

# 输入120 返回 1+2+0
# s = 0
# a = input('please write num:')
# a = list(a)

# for i in str(a):
#     s += int(i)
# print(s)
# for i in range(1, len(a)):
#     for j in range(0, i):
#         if a[j] < a[j+1]:
#             a[j], a[j+1] = a[j + 1], a[j]
# print(a)
# 装饰器  递归  lambda

# 闭包  内部函数引用外部函数变量  保证数据安全
def sun():
    li = []

    def inn(val):
        li.append(val)
        return sum(li) / len(li)

    return inn


ab = sun()
print(ab(2000))
print(ab(3000))
print(ab(4000))

# pandas pandas两种主要数据结构 Series对象 和 DataFrame对象
# Series 对象类似字典  键值形式
import pandas as pd

# 冒泡

# ar = [10, 20, 5, 6, 8, 25, 58, 30]
# for i in range(0, len(ar) - 1):
#     for j in range(0, len(ar) - 1 - i):
#         if ar[j] > ar[j + 1]:
#             ar[j], ar[j + 1] = ar[j + 1], ar[j]
# print(ar)


# 递归阶乘
# 2！= 1 * 2   n * (n - 1)
# def factor(n):
#     if n < 2:
#         return 1
#     return n * factor(n - 1)
#
#
# sumf = 0
# for i in range(1, 11):
#     sumf += factor(i)
#
# print(sumf)


# 有效的括号   给定一个只包括'(' ')' '[' ']' '{' '}' , 判断字符串是否有效

# def valid_str(st):
#     if len(st) % 2 == 1:  # 有效的括号都是对，不成对为奇数
#         return False
#     while '()' in st or '[]' in st or '{}' in st:
#         st = st.replace('()', '')
#         st = st.replace('{}', '')
#         st = st.replace('[]', '')
#     return st == ''
#
#
# print(valid_str('([{}])'))
#
# for i in range(1, 10):
#     if i % 3 == 0:
#         break
#     print(5 % 3)

# 装饰器
print('------------------------------------------------------------')
import time


def ptime(func):
    def f(maxnum):
        s1 = time.time()
        res = func(maxnum)
        s2 = time.time()
        print('耗时总计：{:.4f}'.format(s2 - s1))
        return res

    return f


def is_prime(num):
    if num < 2:
        return False
    elif num == 2:
        return True
    else:
        for i in range(2, num):
            if num % i == 0:
                return False
        return True


@ptime
def prime(maxnum):
    pcount = 0
    for i1 in range(2, maxnum):
        if is_prime(i1):
            pcount += 1
    return pcount


c = prime(10000)
print(c)


# 统计函数使用次数的装饰器  比如 f1()使用了2次  f2()使用了3次

def f(func):
    def f3():
        func()
        a = 1
        return a

    return f3


@f
def f1():
    print('我是f1-f1-f1')


@f
def f2():
    print('我是f2-f2-f2')


def f4(num):
    a = 0
    b = 0
    for i in range(num):
        res = f1()
        a += res
        if i >= 3:
            res2 = f2()
            b += res2
    print(a, b)


f4(20)
